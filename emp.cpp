#include<iostream>
using namespace std;

struct Employee{
  string fname, lname, email, num;
  double id , salary;
  Employee *manager;
};

void emp_details(Employee e)
{
  cout<<"\n\nName: "<<e.fname<<" "<<e.lname
      <<"\nID: "<<e.id
      <<"\nEmail: "<<e.email
      <<"\nNumber: "<<e.num
      <<"\nSalary: "<<e.salary;
  if(e.manager != nullptr)
    cout<<"\nManager: "<<e.manager->fname
	<<" "<<e.manager->lname;
}
int main()
{
  Employee M={"hatim","fahmi","hatim@gmial.com","00000000",1,6000,nullptr};
  Employee s1={"abbas","joe","jowe@gmial.com","018513232",2,3500,&M};
  Employee s2={"saif","adeeed","saif99@gmial.com","45443220",3,40000,&M};
  cout<<"Employee details: ";
  emp_details(M);
  emp_details(s1);
  emp_details(s2);
}
  
  
