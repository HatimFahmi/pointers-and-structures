#include<cstring>
#include<iostream>
using namespace std;
void reverse(char s[])
{
  const size_t len = strlen(s);

    for(size_t i=0; i<len/2; i++)
        swap(s[i], s[len-i-1]);
}
int main()
{
  char s[50];
  cout<<"Enter string: ";
  cin.getline(s,50);
  reverse(s);
  cout<<s;
}
